const findAllTransactions = require('./findAllTransactions');

function getTransactionById(id, onDone) {

  findAllTransactions((err, transactions) => {

    if (err) return onDone(err);

    const transaction = transactions.find(transaction=> transaction.id == id);

    onDone(null, transaction);

  });
}

module.exports = getTransactionById;
