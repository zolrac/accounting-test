const { readFile } = require('fs');
const { DB }       = require('common/env');

const Encoding = "utf8";

function findAllTransactions(onDone) {

  readFile( DB, Encoding, (err, data)=> {

    if (err) return err.code == 'ENOENT' ? onDone(null, []) : onDone(err);

    const transactions = JSON.parse(data);

    onDone(null, transactions);

  });

}

module.exports = findAllTransactions;
