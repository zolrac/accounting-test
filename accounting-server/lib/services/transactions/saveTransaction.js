const { writeFile } = require('fs');
const { DB }        = require('common/env');

const findAllTransactions = require('./findAllTransactions');

function saveTransaction( transaction, onDone) {

  findAllTransactions((err, transactions)=> {

    const data = [...transactions, transaction];

    writeFile( DB, JSON.stringify( data ), err=>{

      if (err) return onDone(err);
      
      onDone(null, transaction);

    });

  });
}

module.exports = saveTransaction;
