const { NotFound }              = require( 'common/api/constants/httpStatusCodes' );
const { UnkownEndpointMessage } = require( 'common/api/constants/messages' );

function unknownEndpoint( _, res ){
  
  res.status( NotFound ).send( UnkownEndpointMessage );

}

module.exports = unknownEndpoint;
