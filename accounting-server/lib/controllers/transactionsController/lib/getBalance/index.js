const {
  Ok,
  InternalServerError
} = require('common/api/constants/httpStatusCodes');

const {
  InternalServerErrorMessage
} = require('common/api/constants/messages');

const getAccountBalance = require('../getAccountBalance');


function getBalance( _, res ) {

  getAccountBalance((err, balance)=> {

    if (err) return res.status(InternalServerError).send(InternalServerErrorMessage);

    res.status(Ok).send({ balance });

  });

}

module.exports = getBalance;
