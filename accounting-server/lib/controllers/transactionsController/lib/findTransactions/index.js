const {
  Ok,
  InternalServerError
} = require('common/api/constants/httpStatusCodes');

const {
  InternalServerErrorMessage
} = require('common/api/constants/messages');

const findAllTransactions = require('services/transactions/findAllTransactions');


function findTransactions( _, res ) {

  findAllTransactions( (err, transactions)=> {

    if (err) return res.status(InternalServerError).send(InternalServerErrorMessage);

    res.status(Ok).send(transactions);

  });

}

module.exports = findTransactions;
