const {
  Ok,
  BadRequest,
  InternalServerError
} = require('common/api/constants/httpStatusCodes');

const {
  InternalServerErrorMessage
} = require('common/api/constants/messages');

const {
  Credit,
  Debit
} = require('../constants');

const ValidTypes = [Credit, Debit];
const InvalidInputMessage = "invalid input";

const generateId = require('common/generateId');
const isNumber = require('common/isNumber');
const saveTransaction = require('services/transactions/saveTransaction');

const getAccountBalance = require('../getAccountBalance');

function createTransaction( req, res ) {

  const { body: {
    type,
    amount
  } } = req;
  
  if (!ValidTypes.includes(type) || !isNumber(amount) || amount <=0 ) return res.status(BadRequest).send(InvalidInputMessage);

  getAccountBalance( (err, balance)=> {

    if (err) return res.status(InternalServerError).send(InternalServerErrorMessage);
    if (type == Debit && balance - amount <= 0) return res.status(BadRequest).send(InvalidInputMessage);

    const transaction = {
      id: generateId(),
      type,
      amount,
      effectiveDate: new Date()
    };

    saveTransaction(transaction, err=> {

      if (err) return res.status(InternalServerError).send(InternalServerErrorMessage);

      res.status(Ok).send(transaction);

    });

  });

}

module.exports = createTransaction;
