const {
  Credit
} = require('./constants');

const findAllTransactions = require('services/transactions/findAllTransactions');


function getAccountBalance(onDone) {

  findAllTransactions((err, transactions) => {

    if (err) return onDone(err);

    const balance = transactions.reduce((acc, { type, amount }) => {

      return type == Credit
        ? acc + amount
        : acc - amount;

    }, 0);

    onDone(null, balance);

  });

}

module.exports = getAccountBalance;
