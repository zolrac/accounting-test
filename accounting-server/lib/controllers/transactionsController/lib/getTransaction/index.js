const {
  Ok,
  NotFound,
  InternalServerError
} = require('common/api/constants/httpStatusCodes');

const {
  InternalServerErrorMessage
} = require('common/api/constants/messages');

const getTransactionById = require('services/transactions/getTransactionById');

const TransactionNotFoundMessage = "transaction not found";


function getTransaction( req, res ) {

  const { params: { id } } = req;

  getTransactionById(id, (err, transaction) => {

    if (err) return res.status(InternalServerError).send(InternalServerErrorMessage);
    if (!transaction) return res.status(NotFound).send(TransactionNotFoundMessage);

    res.status(Ok).send(transaction);

  });

}

module.exports = getTransaction;
