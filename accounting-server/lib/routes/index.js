const { Router } = require( 'express' );
const router     = Router();

const transactions = require( './transactions' );

const rootController = require( '../controllers/rootController' );
const { getBalance } = require('../controllers/transactionsController');

router.use('/transactions', transactions );

// root endpoint
router.get('/', getBalance );

// unknown endpoints
router.get( '*', rootController.unknownEndpoint );
router.post( '*', rootController.unknownEndpoint );
router.put( '*', rootController.unknownEndpoint );
router.delete( '*', rootController.unknownEndpoint );

module.exports = router;
