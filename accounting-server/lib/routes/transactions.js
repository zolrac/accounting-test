const { Router } = require('express');
const router     = Router();

const transactionsController = require('../controllers/transactionsController');

// list of transactions
router.get('/', transactionsController.findTransactions);

// create a transaction
router.post('/', transactionsController.createTransaction);

// get a specifictly transaction
router.get('/:id', transactionsController.getTransaction);

module.exports = router;
