const express    = require('express');
const bodyParser = require('body-parser');

const {
  AccessControlAllowOrigin,
  AccessControlAllowHeaders,
  Origin,
  XRequestedWith,
  ContentType,
  Accept } = require('common/api/constants/headers');

const {
  InternalServerError
} = require('common/api/constants/httpStatusCodes');

const {
  InternalServerErrorMessage
} = require('common/api/constants/messages');

const AllowedHeaders = `${Origin}, ${XRequestedWith}, ${ContentType}, ${Accept}`;

const env = require('common/env');

const routes = require('./lib/routes');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((_, res, next) => {

  res.header(AccessControlAllowOrigin, '*');
  res.header(AccessControlAllowHeaders, AllowedHeaders);

  next();

});

app.use((err, _, res, next)=> {
  
  res.status(InternalServerError).send(InternalServerErrorMessage);

});

console.info('================= Environment =======================');
console.info(`NODE_ENV: ${env.NODE_ENV}`);
console.info(`DB: ${env.DB}`);
console.info('=====================================================');

app.use('/', routes);

module.exports = app;
