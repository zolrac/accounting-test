import React from "react";
import { Route, Switch, withRouter, Link } from "react-router-dom";
import { v4 as uuid } from "uuid"

import List from "./List";
import Item from "./Item";
import "./App.css";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";

function App() {
  return (
    <Container>
      <Nav style={{margin: "1rem 0"}}>
        <Nav.Item style={{marginRight: 10}}>
          <Link to="/">Transactions</Link>
        </Nav.Item>
      </Nav>

      <Switch>
        <Route exact path="/">
          <List key={uuid()}/>
        </Route>
        <Route path="/:id">
          <Item />
        </Route>
      </Switch>
    </Container>
  );
}

export default withRouter(App);
