import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

import {Form, Col} from "react-bootstrap";

function Item() {
  const { id } = useParams();

  const [item, setItem] = useState("");
  
  useEffect(() => {
    if (!item) {
      getItem();
    }
  }, [item]); //eslint-disable-line react-hooks/exhaustive-deps

  const getItem = async () => {
    const response = await axios.get(
      `http://localhost:3001/transactions/${id}`
    );

    setItem(response.data);
  };

  return (
    <Form>
      <Form.Group>
        <Form.Row>
          <Form.Label column="lg" lg={2}>
            id
          </Form.Label>
          <Col>
            <Form.Label column="lg">{item.id}</Form.Label>
          </Col>
        </Form.Row>
        <Form.Row>
          <Form.Label column="lg" lg={2}>
            type
          </Form.Label>
          <Col>
            <Form.Label column="lg">{item.type}</Form.Label>
          </Col>
        </Form.Row>
        <Form.Row>
          <Form.Label column="lg" lg={2}>
            amount
          </Form.Label>
          <Col>
            <Form.Label column="lg">{item.amount}</Form.Label>
          </Col>
        </Form.Row>
        <Form.Row>
          <Form.Label column="lg" lg={2}>
            effective Date
          </Form.Label>
          <Col>
            <Form.Label column="lg">{item.effectiveDate}</Form.Label>
          </Col>
        </Form.Row>
      </Form.Group>
    </Form>
  );
}

export default Item;
