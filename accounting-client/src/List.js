import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import Table from "react-bootstrap/Table";

function List({ type }) {
  const [list, setList] = useState("");

  useEffect(() => {
    if (!list) {
      getList();
    }
  }, [list]);

  const getList = async () => {
    const response = await axios.get(
      `http://localhost:3001/transactions`
    );

    setList(response.data);
  };

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>id</th>
          <th>Type</th>
          <th>Amount</th>
          <th>Effective Date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {list &&
          list.map(({ id, type, amount, effectiveDate }) => (
            <tr key={id}>
              <td>{id}</td>
              <td>{type}</td>
              <td>{amount}</td>
              <td>{effectiveDate}</td>
              <td><Link to={`/${id}`}>View Transaction</Link></td>
            </tr>
          ))}
      </tbody>
    </Table>
  );
}

export default List;
